import Common._
import Dependencies._

ThisBuild / version := "1.0.0"

ThisBuild / scalaVersion := versionScala

ThisBuild / scalafmtOnCompile := true

ThisBuild / shellPrompt := (s => Project.extract(s).currentProject.id + " > ")

lazy val root = Project("scala-web-development", file(".")).aggregate(
  httpTest,
  `config-discovery`,
  `engineering-guice`,
  `web-client`,
  `oauth-server`,
  oauth,
  `user-server`,
  monitor,
  data,
  foundation,
  database,
  common)

lazy val book = project
  .in(file("book"))
  .enablePlugins(ParadoxMaterialThemePlugin)
  .dependsOn(
    `config-discovery`,
    `engineering-guice`,
    monitor,
    httpTest,
    oauth,
    foundation,
    database,
    common % "compile->compile;test->test")
  .settings(
    Compile / paradox / name := "Scala Web Development",
    Compile / paradoxMaterialTheme ~= {
      _.withLanguage(java.util.Locale.SIMPLIFIED_CHINESE)
        .withColor("teal", "indigo")
        .withRepository(uri("https://github.com/yangbajing/scala-web-development"))
        .withSocial(
          uri("https://github.com/yangbajing"),
          uri("https://weibo.com/yangbajing"),
          uri("https://www.yangbajing.me/"))
    },
    paradoxProperties ++= Map(
        "github.base_url" -> s"https://github.com/yangbajing/scala-web-development/tree/${version.value}",
        "extref.rfc.base_url" -> "http://tools.ietf.org/html/rfc%s",
        "image.base_url" -> ".../assets/imgs",
        "scala.version" -> scalaVersion.value,
        "scala.binary_version" -> scalaBinaryVersion.value,
        "scaladoc.akka.base_url" -> s"http://doc.akka.io/api/$versionAkka",
        "akka.version" -> versionAkka),
    libraryDependencies ++= Seq(_akkaHttpTestkit))

lazy val httpTest = project
  .in(file("http-test"))
  .dependsOn(common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(libraryDependencies ++= _slicks)

lazy val `config-discovery` = project
  .in(file("config-discovery"))
  .enablePlugins(AkkaGrpcPlugin, JavaAgent)
  .dependsOn(common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(
    javaAgents += "org.mortbay.jetty.alpn" % "jetty-alpn-agent" % "2.0.9" % "runtime;test",
    assembly / mainClass := Some("scalaweb.discovery.server.Application"),
    assembly / test := {},
    libraryDependencies ++= Seq(_scalapb, _akkaPersistence) ++ _akkaClusters)

lazy val `ant-design-pro` = project
  .in(file("ant-design-pro"))
  .dependsOn(common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(
    assembly / mainClass := Some("scalaweb.ant.design.pro.Main"),
    assembly / test := {},
    libraryDependencies ++= Seq(_rhino))

lazy val `engineering-guice` = project
  .in(file("engineering-guice"))
  .dependsOn(common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(
    assembly / mainClass := Some("scalaweb.enginnering.guice.boot.Main"),
    assembly / test := {},
    libraryDependencies ++= Seq(_guice, _guiceAssistedinject))

lazy val monitor = project
  .in(file("monitor"))
  .dependsOn(common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(
    assembly / mainClass := Some("scalaweb.monitor.boot.Main"),
    assembly / test := {},
    libraryDependencies ++= Seq() ++ _kamons)

lazy val `web-client` = project
  .in(file("web-client"))
  .dependsOn(oauth, common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(libraryDependencies ++= Seq())

lazy val `oauth-server` = project
  .in(file("oauth-server"))
  .enablePlugins(AkkaGrpcPlugin)
  .dependsOn(`oauth-grpc-infra`, oauth, common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(
    akkaGrpcCodeGeneratorSettings := akkaGrpcCodeGeneratorSettings.value.filterNot(_ == "flat_package"),
    libraryDependencies ++= Seq(_redisclient))

lazy val `oauth-grpc-infra` = project
  .in(file("oauth-grpc-infra"))
  .enablePlugins(AkkaGrpcPlugin)
  .dependsOn(`user-grpc-protobuf` % "compile->compile;protobuf-src->protobuf-src")
  .settings(
    akkaGrpcGeneratedSources := Seq(AkkaGrpc.Client),
    akkaGrpcCodeGeneratorSettings += "server_power_apis",
    akkaGrpcCodeGeneratorSettings := akkaGrpcCodeGeneratorSettings.value.filterNot(_ == "flat_package"))

lazy val data = project
  .in(file("data"))
  .dependsOn(common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(libraryDependencies ++= Seq(_akkaHttpJackson))

lazy val `user-server` = project
  .in(file("user-server"))
  .dependsOn(`user-grpc-api`, oauth, common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(libraryDependencies ++= Seq(_redisclient))

lazy val `user-grpc-api` = project
  .in(file("user-grpc-api"))
  .enablePlugins(AkkaGrpcPlugin)
  .settings(
    akkaGrpcCodeGeneratorSettings := akkaGrpcCodeGeneratorSettings.value.filterNot(_ == "flat_package"),
    libraryDependencies ++= Seq("me.yangbajing" %% "user-grpc-protobuf" % version.value % "protobuf-src"))

lazy val `user-grpc-protobuf` = project
  .in(file("user-grpc-protobuf"))
  .settings(
    organization := "me.yangbajing",
    autoScalaLibrary := false,
    Compile / unmanagedSourceDirectories := (Compile / javaSource).value :: Nil,
    Test / unmanagedSourceDirectories := (Test / javaSource).value :: Nil)

lazy val oauth = project
  .in(file("oauth"))
  .dependsOn(common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(libraryDependencies ++= Seq(_redisclient, _jwtCore))

lazy val foundation = project
  .in(file("foundation"))
  .dependsOn(common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(libraryDependencies ++= Seq(_redisclient, _alpakkaCassandra))

lazy val database = project
  .in(file("database"))
  .dependsOn(common % "compile->compile;test->test")
  .settings(basicSettings: _*)
  .settings(libraryDependencies ++= Seq())

lazy val common = project
  .in(file("common"))
  .settings(basicSettings: _*)
  .settings(
    libraryDependencies ++= Seq(
        _bouncycastleProvider,
        _postgresql,
        _hikariCP,
        "com.thesamet.scalapb" %% "scalapb-runtime" % scalapb.compiler.Version.scalapbVersion % "protobuf",
        _config,
        _akkaHttpTestkit % Test,
        _scalaCollectionCompat,
        _scalaJava8Compat) ++ _akkas ++ _akkaHttps ++ _logs,
    Compile / PB.targets := Seq(scalapb.gen(flatPackage = true) -> (Compile / sourceManaged).value))
