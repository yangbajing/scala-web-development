
$(function (){
  var params = Qs.parse(location.search.slice(1));
  var text = JSON.stringify(params, null, 2)
  $('#querystring').html(text);

  $('input[name=redirect_uri]').val(params.redirect_uri);
})
