package scalaweb.oauth.client.application.service

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.{ Authorization, BasicHttpCredentials }
import akka.http.scaladsl.model.{ FormData, HttpMethods, HttpRequest }
import akka.http.scaladsl.unmarshalling.Unmarshal
import com.typesafe.scalalogging.StrictLogging
import scalaweb.oauth.model.{ AccessToken, GrantType }

import scala.concurrent.Future

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-21 13:46:30
 */
class OAuth2ClientService()(implicit system: ActorSystem[_]) extends StrictLogging {
  import system.executionContext

  def authCallback(code: String, state: String): Future[AccessToken] = {
    import helloscala.http.JacksonSupport._
    val request = HttpRequest(
      HttpMethods.POST,
      "http://localhost:33333/oauth2/token",
      headers = Seq(Authorization(BasicHttpCredentials("client-id", "client-secret"))),
      entity = FormData(
        "grant_type" -> GrantType.AUTHORIZATION_CODE.VALUE,
        "code" -> code,
        "redirect_uri" -> "http://localhost:33334/user/profile").toEntity)
    Http().singleRequest(request).flatMap(response => Unmarshal(response.entity).to[AccessToken])
  }
}
