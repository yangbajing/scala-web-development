package scalaweb.oauth.client.endpoint.route

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Route
import helloscala.http.route.AbstractRoute
import scalaweb.oauth.client.application.service.OAuth2ClientService

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-21 13:39:45
 */
class ClientRoute(oauth2ClientService: OAuth2ClientService)(implicit system: ActorSystem[_]) extends AbstractRoute {
  override def route: Route = pathPrefix("oauth2") {
    callbackRoute
  }

  def callbackRoute: Route = pathGet("callback") {
    parameters("code", "state".?("")) { (code, state) =>
      futureComplete(oauth2ClientService.authCallback(code, state))
    }
  }

  def htmlRoute: Route = //getFromResourceDirectory("html")
    getFromDirectory("oauth-client/web")
}
