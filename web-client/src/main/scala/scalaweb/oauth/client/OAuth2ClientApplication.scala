package scalaweb.oauth.client

import akka.actor.typed.{ ActorSystem, SpawnProtocol }
import akka.http.scaladsl.Http
import com.typesafe.scalalogging.StrictLogging
import scalaweb.oauth.client.application.service.OAuth2ClientService
import scalaweb.oauth.client.endpoint.route.ClientRoute

import scala.util.{ Failure, Success }

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-21 13:38:52
 */
object OAuth2ClientApplication extends App with StrictLogging {
  implicit val system = ActorSystem(SpawnProtocol(), "oauth-client")
  import system.executionContext

  val host = "127.0.0.1"
  val port = 33334

  val oauth2ClientService = new OAuth2ClientService()
  val handler = new ClientRoute(oauth2ClientService).route

  Http().newServerAt(host, port).bind(handler).onComplete {
    case Success(binding) =>
      logger.info(s"OAuth 2 service startup success, $binding")
    case Failure(cause) =>
      logger.error("start failure", cause)
      shutdown()
  }

  private def shutdown(): Unit = {
    system.terminate()
  }
}
