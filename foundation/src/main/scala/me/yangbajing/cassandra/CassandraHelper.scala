package me.yangbajing.cassandra

import com.datastax.oss.driver.api.core.CqlSession

import scala.jdk.CollectionConverters._

case class CassandraConf(
    nodes: Seq[String],
    clusterName: String,
    username: Option[String],
    password: Option[String],
    keyspace: Option[String])

object CassandraHelper {
  def getCluster(nodes: Seq[String], clusterName: String = "Test Cluster"): CqlSession =
    getCluster(CassandraConf(nodes, clusterName, None, None, None))

  /**
   * 获得 Cassandra 连接 Cluster
   */
  def getCluster(c: CassandraConf): CqlSession = {
    val cluster = CqlSession
      .builder()
      .addContactPoints(c.nodes.map(java.net.InetSocketAddress.createUnresolved(_, 8888)).asJava)
      .withLocalDatacenter(c.clusterName)
      .build()
    cluster
  }
}
