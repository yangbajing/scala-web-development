package me.yangbajing.cassandra

import akka.actor.typed.ActorSystem
import akka.stream.alpakka.cassandra.scaladsl.{ CassandraSession, CassandraSessionRegistry }
import com.datastax.oss.driver.api.core.CqlSession
import com.datastax.oss.driver.api.core.cql.PreparedStatement
import com.typesafe.scalalogging.StrictLogging

import java.util

/**
 * Created by yangbajing(yangbajing@gmail.com) on 2017-04-24.
 */
private class CqlCache(session: CqlSession) extends StrictLogging {
  private val map = new util.HashMap[String, PreparedStatement]()

  def putIfAbsent(cql: String): PreparedStatement = synchronized {
    if (map.containsKey(cql)) {
      map.get(cql)
    } else {
      logger.debug("new cql: {}", cql)
      val pstmt = session.prepare(cql)
      map.put(cql, pstmt)
      pstmt
    }
  }
}

/**
 * Created by yangbajing(yangbajing@gmail.com) on 2017-03-22.
 */
class StandaloneCassandraSession()(implicit val session: RichCassandraSession, system: ActorSystem[_]) {
  private lazy val cqlCache = new CqlCache(session.session)

  implicit val cassandraSession: CassandraSession =
    CassandraSessionRegistry(system).sessionFor("cassandra")

  /**
   * 生成预编译 CQL 语句
   *
   * @param cql Cassandra CQL 语句
   * @return 若存在就直接返回，不存在则生成返回并缓存
   */
  def prepare(cql: String): PreparedStatement = cqlCache.putIfAbsent(cql)

  def close(): Unit =
    if (session != null) {
      session.session.close()
    }
}

class RichCassandraSession(val session: CqlSession)
