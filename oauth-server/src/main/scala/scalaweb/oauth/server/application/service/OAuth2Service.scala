package scalaweb.oauth.server.application.service

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.headers.Authorization
import com.typesafe.scalalogging.StrictLogging
import helloscala.common.exception.{ HSBadRequestException, HSNotFoundException, HSUnauthorizedException }
import scalaweb.oauth.model._
import scalaweb.oauth.{ AccessSession, LoginReq, LoginResp }

import java.util.UUID
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

class OAuth2Service()(implicit system: ActorSystem[_]) extends StrictLogging {
  import system.executionContext

  // Key: code, Value: UserInfo
  private var cachedCodes = Map[String, UserInfo]()

  private var cachedTokens = Map[String, Principle]()

  // Key: refresh token, Value: access tokens
  private var cachedRefreshTokens = Map[String, Set[String]]()

  // TODO 存储到带 TTL 的外部缓存系统
  private var cachedAuthorizeReqs = Map[String, AuthorizeReq]()

  def login(req: LoginReq): Future[LoginResp] = Future {
    LoginResp(createToken())
  }

  def getAuthorizeReqById(oauth2ReqId: String): Future[AuthorizeReq] = Future {
    cachedAuthorizeReqs.getOrElse(oauth2ReqId, throw HSBadRequestException("Authorize Req is not found."))
  }

  /**
   * 会话令牌
   * @param token
   * @return
   */
  def getAccessSession(token: String): Future[AccessSession] = Future {
    AccessSession("1", 7.days.toMillis, System.currentTimeMillis()).ensuring { as =>
      logger.debug(s"生成会话令牌：$as")
      true
    }
  }

  def cacheAuthorize(req: AuthorizeReq): String = {
    val oauth2ReqId = UUID.randomUUID().toString
    cachedAuthorizeReqs = cachedAuthorizeReqs.updated(oauth2ReqId, req)
    oauth2ReqId
  }

  /**
   * 检查访问领牌是否有效
   * @param token access token
   * @return true：有效，false：无效
   */
  def checkToken(token: String): Boolean = cachedTokens.contains(token)

  /**
   * 生成访问令牌
   * @param formData
   * @param authorization
   * @return
   */
  def generateToken(formData: Map[String, String], authorization: Option[Authorization]): Future[AccessToken] = {
    val tokenReq = formData
      .get("oauth2_req_id")
      .map { oauth2ReqId =>
        val req = cachedAuthorizeReqs.getOrElse(
          oauth2ReqId,
          throw HSNotFoundException(s"The AuthorizeReq not found, the incoming is '$oauth2ReqId'."))
        val data = formData ++ Seq(
            "state" -> req.state,
            "redirect_uri" -> req.redirectUri,
            "response_type" -> req.responseType.VALUE)
        AccessTokens.toGenerateTokenReq(data, authorization)
      }
      .getOrElse(AccessTokens.toGenerateTokenReq(formData, authorization))
    tokenReq.grantType match {
      case GrantType.REFRESH_TOKEN      => refreshToken(tokenReq.toGenerateRefreshTokenReq)
      case GrantType.AUTHORIZATION_CODE => accessTokenForAuthorization(tokenReq.toGenerateAccessTokenReq)
      case GrantType.CLIENT_CREDENTIALS => accessTokenForClient(tokenReq.toGenerateAccessTokenReq)
    }
  }

  /**
   * 刷新访问领牌。
   * 若原 access_token 未过期则重置期过期时期，不需要生成一个新的 access_token
   * @param req 刷新令牌请求
   * @return [[AccessToken]]
   */
  private def refreshToken(req: GenerateRefreshTokenReq): Future[AccessToken] = Future {
    val maybeAccessToken = for {
      accessTokens <- cachedRefreshTokens.get(req.refreshToken)
      principle <- accessTokens.view.flatMap(cachedTokens.get).headOption
    } yield {
      val token = createToken()
      cachedTokens = cachedTokens.updated(token, principle)
      AccessToken(token, refreshToken = "").ensuring { at =>
        logger.debug(s"刷新 AccessToken 成功：$at")
        true
      }
    }

    maybeAccessToken.getOrElse(
      throw HSUnauthorizedException(s"The refresh token is invalid, the incoming value is ${req.refreshToken}."))
  }

  private def accessTokenForClient(req: GenerateAccessTokenReq): Future[AccessToken] = Future {
    // TODO 从仓储里获取并判断 client secret 是否有效
    val clientDetail = ClientDetail(req.clientId, req.clientSecret)

    createAccessToken(clientDetail)
  }

  private def accessTokenForAuthorization(req: GenerateAccessTokenReq): Future[AccessToken] = Future {
    cachedCodes.get(req.code) match {
      case Some(userInfo) => createAccessToken(userInfo)
      case _              => throw HSUnauthorizedException(s"Invalid code, the incoming is ${req.code}.")
    }
  }

  private def createAccessToken(principle: Principle): AccessToken = {
    val accessToken = createToken()
    val refreshToken = createToken()
    cachedTokens = cachedTokens.updated(accessToken, principle)
    cachedRefreshTokens = cachedRefreshTokens.updated(refreshToken, Set(accessToken))
    AccessToken(accessToken, refreshToken)
  }

  def authorizeApprove(req: AuthorizeApproveReq): Future[AuthorizeApproveResp] = Future {
    val code = UUID.randomUUID().toString
    val authorizeReq = cachedAuthorizeReqs.get(req.oauth2ReqId) match {
      case Some(value) => value
      case _           => throw new IllegalArgumentException
    }

    val userInfo = UserInfo(authorizeReq.clientId, authorizeReq.clientId, "yangbajing")
    cachedCodes = cachedCodes.updated(code, userInfo)

    var redirectUri: Uri = authorizeReq.redirectUri
    val query = ("code" -> code) +: ("state" -> authorizeReq.state) +: redirectUri.query()
    redirectUri = redirectUri.withQuery(query)
    AuthorizeApproveResp(redirectUri.toString()).ensuring { resp =>
      logger.debug(s"The generated AuthorizeApproveResp is $resp")
      true
    }
  }

  def createToken(): String = UUID.randomUUID().toString
}
