package scalaweb.oauth.server

import akka.actor.typed.{ ActorSystem, SpawnProtocol }
import akka.http.scaladsl.Http
import com.typesafe.scalalogging.StrictLogging
import scalaweb.oauth.server.application.service.OAuth2Service
import scalaweb.oauth.server.endpoint.route.OAuth2Route

import scala.util.{ Failure, Success }

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-21 11:36:44
 */
object OAuth2ServerApplication extends App with StrictLogging {
  implicit val system = ActorSystem(SpawnProtocol(), "oauth-server")
  import system.executionContext

  sys.addShutdownHook { shutdown() }

  val host = "127.0.0.1"
  val port = 33333

  val service = new OAuth2Service
  val handler = new OAuth2Route(service).route

  Http().newServerAt(host, port).bind(handler).onComplete {
    case Success(binding) =>
      logger.info(s"OAuth 2 service startup success, $binding")
    case Failure(cause) =>
      logger.error("start failure", cause)
      shutdown()
  }

  private def shutdown(): Unit = {
    system.terminate()
  }
}
