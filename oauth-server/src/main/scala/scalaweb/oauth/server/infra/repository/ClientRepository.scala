package scalaweb.oauth.server.infra.repository

import akka.actor.typed.ActorSystem
import scalaweb.oauth.model.ClientDetail

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-18 20:52:15
 */
class ClientRepository()(implicit system: ActorSystem[_]) {
  private var clients = Map[String, ClientDetail]()
}
