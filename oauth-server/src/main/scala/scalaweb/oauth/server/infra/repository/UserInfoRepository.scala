package scalaweb.oauth.server.infra.repository

import akka.actor.typed.ActorSystem

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-18 20:52:29
 */
class UserInfoRepository()(implicit system: ActorSystem[_]) {}
