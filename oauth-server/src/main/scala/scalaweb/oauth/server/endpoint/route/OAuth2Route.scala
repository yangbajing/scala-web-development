package scalaweb.oauth.server.endpoint.route

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.MediaTypes.`application/json`
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{ Authorization, HttpCookie, Location }
import akka.http.scaladsl.server.Route
import com.fasterxml.jackson.databind.ObjectMapper
import com.typesafe.scalalogging.StrictLogging
import helloscala.common.json.Jackson
import helloscala.http.route.AbstractRoute
import scalaweb.oauth.LoginReq
import scalaweb.oauth.common.OAuthConstants
import scalaweb.oauth.model.{ Approve, AuthorizeApproveReq }
import scalaweb.oauth.server.application.service.OAuth2Service

import scala.concurrent.duration._

class OAuth2Route(val oauth2Service: OAuth2Service)(implicit system: ActorSystem[_])
    extends AbstractRoute
    with OAuth2Directive
    with StrictLogging {
  implicit val objectMapper: ObjectMapper =
    akka.serialization.jackson.JacksonObjectMapperProvider(system).getOrCreate("jackson-json", None)

  override def route: Route =
    pathPrefix(separateOnSlashes(OAuthConstants.PATH_PREFIX)) {
      tokenRoute ~
      getAuthorizeRoute ~
      postAuthorizePostRoute ~
      approvePageRoute ~
      loginPageRoute ~
      loginRoute ~
      checkTokenRoute
    } ~
    pathPrefix("resource") {
      pathGet("authorize" / Segment) { oauth2ReqId =>
        futureComplete(oauth2Service.getAuthorizeReqById(oauth2ReqId))
      }
    } ~
    path("user" / "profile") {
      extractRequest { request =>
        complete(request.toString())
      }
    } ~
    htmlRoute

  def getAuthorizeRoute: Route = pathGet("authorize") {
    extractAuthorizeReq { req =>
      optionalAccessSession { optionalSession =>
        val oauth2ReqId = oauth2Service.cacheAuthorize(req)

        // XXX 校验用户是否已登录
        //      已登录：跳转/显示到授权批准页面
        //      未登录：跳转到登录页面，登录成功后再跳转回授权批准页面
        val uri: Uri = if (optionalSession.isDefined) {
          s"/${OAuthConstants.PATH_PREFIX}/approve?oauth2_req_id=$oauth2ReqId"
        } else {
          val redirectUri = s"/${OAuthConstants.PATH_PREFIX}/approve?oauth2_req_id=$oauth2ReqId"
          Uri(s"/${OAuthConstants.PATH_PREFIX}/login").withQuery(Uri.Query("redirect_uri" -> redirectUri))
        }

        val response = HttpResponse(StatusCodes.Found, headers = List(Location(uri)))
        complete(response)
      }
    }
  }

  def postAuthorizePostRoute: Route = pathPost("authorize") {
    formFields(
      "oauth2_req_id",
      "approve".as[Approve],
      "scope".as[Set[String]].?(OAuthConstants.SCOPE_ALL),
      "isJson".as[Boolean].?(false)) { (oauth2ReqId, approve, scopes, isJson) =>
      val req = AuthorizeApproveReq(oauth2ReqId, scopes, approve)
      onSuccess(oauth2Service.authorizeApprove(req)) {
        case resp if isJson =>
          complete(HttpEntity(`application/json`, Jackson.defaultObjectMapper.writeValueAsString(resp)))
        case resp =>
          complete(HttpResponse(StatusCodes.Found, headers = List(Location(resp.redirectUri))))
      }
    }
  }

  def approvePageRoute: Route = pathGet("approve") {
    getFromFile("oauth-server/web/oauth2/approve.html")
  }
  def loginPageRoute: Route = pathGet("login") {
    getFromFile("oauth-server/web/oauth2/login.html")
  }

  def tokenRoute: Route = pathPost("token" | "access_token") {
    optionalHeaderValueByType(Authorization) { authorization =>
      entity(as[FormData]) { formData =>
        futureComplete(oauth2Service.generateToken(formData.fields.toMap, authorization))
      }
    }
  }

  def loginRoute: Route = pathPost("login") {
    formFields("username", "password", "redirect_uri", "oauth2_req_id").as(LoginReq) { req =>
      onSuccess(oauth2Service.login(req)) { resp =>
        val cookie =
          HttpCookie("fc-session", resp.token, maxAge = Some(7.days.toSeconds), path = Some("/"), httpOnly = true)
        setCookie(cookie) {
          complete(HttpResponse(StatusCodes.Found, List(Location(req.redirectUri))))
        }
      }
    }
  }

  def checkTokenRoute: Route = pathGet("check_token") {
//    optionalAccessToken {
//      case Some(accessToken) =>
//        onSuccess(oauth2Service.checkToken(accessToken)) {
//          case true => complete(StatusCodes.OK)
//          case _    => complete(StatusCodes.Unauthorized)
//        }
//      case _ => reject(AuthRejection("参数'access_token'缺失"))
//    }
    ???
  }

  def htmlRoute: Route = getFromDirectory("oauth-server/web")

  def resourceRoute: Route = pathPrefix("resource") {
//    authorize()
    ???
  }
}
