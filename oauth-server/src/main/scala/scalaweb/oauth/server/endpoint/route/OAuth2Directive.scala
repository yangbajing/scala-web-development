package scalaweb.oauth.server.endpoint.route

import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.headers.OAuth2BearerToken
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ Directive1, MissingHeaderRejection }
import akka.http.scaladsl.unmarshalling.{ FromStringUnmarshaller, Unmarshaller }
import helloscala.http.route.AbstractRoute
import scalaweb.oauth.AccessSession
import scalaweb.oauth.common.OAuthConstants
import scalaweb.oauth.model._
import scalaweb.oauth.server.application.service.OAuth2Service

import scala.util.{ Failure, Success }

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-20 19:21:27
 */
trait OAuth2Directive {
  this: AbstractRoute =>
  val oauth2Service: OAuth2Service

  implicit def grantTypeFromStringUnmarshaller: FromStringUnmarshaller[GrantType] =
    Unmarshaller.strict[String, GrantType](GrantType.string2GrantType)

  implicit def responseTypeFromStringUnmarshaller: FromStringUnmarshaller[ResponseType] =
    Unmarshaller.strict[String, ResponseType](ResponseType.string2ResponseType) //

  implicit def approveFromStringUnmarshaller: FromStringUnmarshaller[Approve] =
    Unmarshaller.strict[String, Approve](Approve.string2Approve)

  implicit def uriFromStringUnmarshaller: FromStringUnmarshaller[Uri] = Unmarshaller.strict[String, Uri](Uri.apply)

//  implicit def setStringFromUnmarshaller: FromStringUnmarshaller[Set[String]] =
//    Unmarshaller.strict[String, Set[String]](text => text.split(' ').view.map(_.trim).filter(_.nonEmpty).toSet)

  def extractAuthorizeReq: Directive1[AuthorizeReq] =
    parameters(
      "response_type".as[ResponseType],
      "client_id",
      "redirect_uri".?(OAuthConstants.DEFAULT_REDIRECT_URI),
      "scope".as[Set[String]].?(OAuthConstants.SCOPE_ALL),
      "state".?("")).as(AuthorizeReq).flatMap { req =>
      provide(req)
    }

  def extractToken: Directive1[String] = extractCredentials.flatMap {
    case Some(OAuth2BearerToken(token)) => provide(token)
    case _                              => reject(MissingHeaderRejection("Authorization"))
  }

  def extractAccessSession: Directive1[AccessSession] = {
    cookie("fc-session")
      .flatMap { cookie =>
        onComplete(oauth2Service.getAccessSession(cookie.value))
      }
      .flatMap {
        case Success(session)   => provide(session)
        case Failure(exception) => reject(AuthRejection(exception.getMessage))
      }
  }

  def optionalAccessSession: Directive1[Option[AccessSession]] = {
//    optionalCookie("fc-session").flatMap {
//      case Some(cookie) =>
//        onComplete(oauth2Service.getAccessSession(cookie.value)).flatMap { tryValue =>
//          provide(tryValue.toOption)
//        }
//      case _ => provide(None)
//    }
    extractAccessSession.map(Some.apply).recover(_ => provide(None))
  }

  //  def optionalAccessToken: Directive1[Option[String]] = extract { ctx =>
  //    val authorization = ctx.request.header[Authorization].flatMap { header =>
  //      header.credentials match {
  //        case OAuth2BearerToken(accessToken) => Some(accessToken)
  //        case _                              => None
  //      }
  //    }
  //    authorization
  //      .orElse(ctx.request.uri.query().get(GrantType.A.name))
  //      .orElse(
  //        ctx.request.headers
  //          .find(_.lowercaseName() == GrantType.access_token.name)
  //          .map(_.value()))
  //  }
  //
  //  def extractAccessSession: Directive1[AccessSession] =
  //    optionalAccessToken.flatMap {
  //      case Some(accessToken) =>
  //        onSuccess(oauth2Service.getAccessSession(accessToken))
  //          .flatMap(accessOwner => provide(accessOwner))
  //      case None => reject(AuthRejection("access_token参数必传"))
  //    }
}
