package scalaweb.oauth.model

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.http.scaladsl.model.{ FormData, HttpEntity, Uri }
import akka.serialization.jackson.JacksonObjectMapperProvider
import helloscala.common.util.StringUtils
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import scalaweb.oauth.common.OAuthConstants

import java.util.UUID

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-21 10:06:07
 */
class ModelTest extends ScalaTestWithActorTestKit with AnyWordSpecLike with Matchers {
  private implicit val objectMapper = JacksonObjectMapperProvider(system).getOrCreate("jackson-json", None)
  "AccessToken" should {
    "AccessToken" in {
      val accessToken = AccessToken(UUID.randomUUID().toString, UUID.randomUUID().toString)
      println(objectMapper.writeValueAsString(accessToken))
    }

    "GenerateAccessTokenReq" in {
      val req = GenerateAccessTokenReq(
        GrantType.AUTHORIZATION_CODE,
        "client-id",
        "client-secret",
        StringUtils.randomString(64),
        "https://wwww.baidu.com")
      println(objectMapper.writeValueAsString(req))
    }
  }

  "Authorize" should {
    "AuthorizeReq" in {
      val req = AuthorizeReq(
        ResponseType.CODE,
        "client-id",
        "https://oauth2.hjgpscm.com/oauth2/callback",
        OAuthConstants.SCOPE_ALL,
        StringUtils.randomString(12))
      println(objectMapper.writeValueAsString(req))
    }

    "test" in {
      val query = Uri.Query("redirect_uri" -> "http://127.0.0.1:33334/oauth2/callback")
      val formData = FormData(query)
      println(FormData(query).toString)
      println(formData.toEntity)
      println(formData.toEntity.asInstanceOf[HttpEntity.Strict].data.utf8String)

      val uri = Uri("http://localhost:33333/oauth2/authorize").withQuery(query)

      println(uri.toRelative)
      println(uri.rawQueryString)
      println(uri.toString())
    }
  }
}
