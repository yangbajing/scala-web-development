package scalaweb.oauth.common

import scala.concurrent.duration._

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-20 13:52:13
 */
object OAuthConstants {
  val SCOPE_ALL = Set("all")
  val PATH_PREFIX = "oauth2"
  val DEFAULT_ACCESS_TOKEN_EXPIRES_SECONDS: Long = 2.hours.toSeconds
  val TOKEN_TYPE_BEARER = "Bearer"
  val DEFAULT_REDIRECT_URI = "/oauth2/callback"
}
