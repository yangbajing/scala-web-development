package scalaweb.oauth.model

import akka.http.scaladsl.model._
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.{ JsonGenerator, JsonParser }
import com.fasterxml.jackson.databind.annotation.{ JsonDeserialize, JsonSerialize }
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.databind.{ DeserializationContext, SerializerProvider }

import scala.language.implicitConversions

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-18 21:05:44
 */
case class AuthorizeReq(
    responseType: ResponseType,
    clientId: String,
    redirectUri: String,
    scopes: Set[String],
    state: String)

/**
 * 授权请求
 * @param oauth2ReqId 服务端缓存中保留的授权请求参数
 * @param scopes 资源拥有者实际批准的权限范围
 */
case class AuthorizeApproveReq(oauth2ReqId: String, scopes: Set[String], approve: Approve)

case class AuthorizeApproveResp(redirectUri: String)

@JsonSerialize(`using` = classOf[Approve.JsonSerializer])
@JsonDeserialize(`using` = classOf[Approve.JsonDeserializer])
sealed abstract class Approve(@JsonProperty val VALUE: String)
object Approve {
  case object APPROVED extends Approve("approved")
  case object REJECTED extends Approve("rejected")

  implicit def string2Approve(value: String): Approve = value match {
    case APPROVED.VALUE => APPROVED
    case REJECTED.VALUE => REJECTED
    case _              => REJECTED
  }

  class JsonSerializer extends StdSerializer[Approve](classOf[Approve]) {
    override def serialize(value: Approve, gen: JsonGenerator, provider: SerializerProvider): Unit =
      gen.writeString(value.VALUE)
  }

  class JsonDeserializer extends StdDeserializer[Approve](classOf[Approve]) {
    override def deserialize(p: JsonParser, ctxt: DeserializationContext): Approve = p.getText
  }
}

object AuthorizeReqs {
  def toAuthorizeReq(request: HttpRequest): AuthorizeReq = {
    val query = if (HttpMethods.GET == request.method) {
      request.uri.query()
    } else if (MediaTypes.`application/x-www-form-urlencoded` == request.entity.contentType.mediaType) {
      val bs = request.entity.asInstanceOf[HttpEntity.Strict].data
      Uri.Query(bs.utf8String)
    } else {
      throw new IllegalArgumentException(s"Invalid request: $request")
    }

    val responseType = query.get("response_type").get
    val clientId = query.get("client_id").get
    val redirectUri = query.getOrElse("redirect_uri", "")
    val state = query.getOrElse("state", "")
    val scopes = query.getOrElse("scope", "").split(' ').view.map(_.trim).filter(_.nonEmpty).toSet

    AuthorizeReq(responseType, clientId, redirectUri, scopes, state)
  }

  def toAuthorizeApproveReq(request: HttpRequest): AuthorizeApproveReq = {
    val query = if (MediaTypes.`application/x-www-form-urlencoded` == request.entity.contentType.mediaType) {
      val bs = request.entity.asInstanceOf[HttpEntity.Strict].data
      Uri.Query(bs.utf8String)
    } else {
      throw new IllegalArgumentException(s"Invalid request: $request")
    }

    val reqid = query.get("reqid").get
    val scopes = query.getOrElse("scope", "").split(' ').view.map(_.trim).filter(_.nonEmpty).toSet
    val approve = query.get("approve").get
    AuthorizeApproveReq(reqid, scopes, approve)
  }
}
