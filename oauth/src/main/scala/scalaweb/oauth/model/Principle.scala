package scalaweb.oauth.model

import java.time.OffsetDateTime

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-20 20:13:37
 */
trait Principle {
  def clientId: String
}

case class UserInfo(clientId: String, userId: String, username: String) extends Principle

case class ClientDetail(clientId: String, clientSecret: String, createTime: OffsetDateTime = OffsetDateTime.now())
    extends Principle
