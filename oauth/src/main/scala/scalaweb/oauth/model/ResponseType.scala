package scalaweb.oauth.model

import com.fasterxml.jackson.core.{ JsonGenerator, JsonParser }
import com.fasterxml.jackson.databind.annotation.{ JsonDeserialize, JsonSerialize }
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.databind.{ DeserializationContext, SerializerProvider }
import helloscala.common.exception.HSBadRequestException

import scala.language.implicitConversions

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-20 18:06:14
 */
@JsonSerialize(`using` = classOf[ResponseType.ResponseTypeJsonSerializer])
@JsonDeserialize(`using` = classOf[ResponseType.ResponseTypeJsonDeserializer])
sealed abstract class ResponseType(val VALUE: String) {
  override def toString: String = VALUE
  def isCode: Boolean
  def isToken: Boolean
}

object ResponseType {
  case object CODE extends ResponseType("code") {
    override val isCode: Boolean = true
    override val isToken: Boolean = false
  }
  case object TOKEN extends ResponseType("token") {
    override val isCode: Boolean = false
    override val isToken: Boolean = true
  }

  implicit def string2ResponseType(value: String): ResponseType = value match {
    case CODE.VALUE  => CODE
    case TOKEN.VALUE => TOKEN
    case other       => throw HSBadRequestException(s"The '$other' response type is invalid.")
  }

  class ResponseTypeJsonSerializer extends StdSerializer[ResponseType](classOf[ResponseType]) {
    override def serialize(value: ResponseType, gen: JsonGenerator, provider: SerializerProvider): Unit =
      gen.writeString(value.VALUE)
  }

  class ResponseTypeJsonDeserializer extends StdDeserializer[ResponseType](classOf[ResponseType]) {
    override def deserialize(p: JsonParser, ctxt: DeserializationContext): ResponseType = p.getText
  }
}
