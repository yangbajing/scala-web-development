package scalaweb.oauth.model

import com.fasterxml.jackson.core.{ JsonGenerator, JsonParser }
import com.fasterxml.jackson.databind.annotation.{ JsonDeserialize, JsonSerialize }
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.databind.{ DeserializationContext, SerializerProvider }
import helloscala.common.exception.HSBadRequestException

import scala.language.implicitConversions

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-18 21:02:47
 */
@JsonSerialize(`using` = classOf[GrantType.JsonSerializer])
@JsonDeserialize(`using` = classOf[GrantType.JsonDeserializer])
sealed abstract class GrantType(val VALUE: String) {
  override def toString: String = VALUE
}

object GrantType {
  case object CLIENT_CREDENTIALS extends GrantType("client_credentials")
  case object AUTHORIZATION_CODE extends GrantType("authorization_code")
  case object REFRESH_TOKEN extends GrantType("refresh_token")
//  case object PASSWORD extends GrantType("PASSWORD")

  implicit def string2GrantType(value: String): GrantType = value match {
    case REFRESH_TOKEN.VALUE      => REFRESH_TOKEN
    case CLIENT_CREDENTIALS.VALUE => CLIENT_CREDENTIALS
    case AUTHORIZATION_CODE.VALUE => AUTHORIZATION_CODE
//    case PASSWORD.VALUE           => throw HSBadRequestException(s"The 'password' grant type is not currently supported.")
    case other => throw HSBadRequestException(s"The '$other' grant type is invalid.")
  }

  class JsonSerializer extends StdSerializer[GrantType](classOf[GrantType]) {
    override def serialize(value: GrantType, gen: JsonGenerator, provider: SerializerProvider): Unit =
      gen.writeString(value.VALUE)
  }

  class JsonDeserializer extends StdDeserializer[GrantType](classOf[GrantType]) {
    override def deserialize(p: JsonParser, ctxt: DeserializationContext): GrantType = p.getText
  }
}
