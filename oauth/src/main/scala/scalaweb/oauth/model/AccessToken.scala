package scalaweb.oauth.model

import akka.http.scaladsl.model.headers.{ Authorization, BasicHttpCredentials }
import com.fasterxml.jackson.annotation.JsonProperty
import helloscala.common.exception.HSBadRequestException
import scalaweb.oauth.common.OAuthConstants

/**
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-18 21:00:19
 */
case class AccessToken(
    @JsonProperty("access_token") accessToken: String,
    @JsonProperty("refresh_token") refreshToken: String,
    @JsonProperty("expires_in") expiresIn: Long = OAuthConstants.DEFAULT_ACCESS_TOKEN_EXPIRES_SECONDS,
    @JsonProperty("token_type") tokenType: String = OAuthConstants.TOKEN_TYPE_BEARER)

trait GenerateTokenReq {
  def grantType: GrantType
  def clientId: String
  def clientSecret: String

  def toGenerateRefreshTokenReq: GenerateRefreshTokenReq = this.asInstanceOf[GenerateRefreshTokenReq]

  def toGenerateAccessTokenReq: GenerateAccessTokenReq = this.asInstanceOf[GenerateAccessTokenReq]
}

case class GenerateAccessTokenReq(
    grantType: GrantType,
    clientId: String,
    clientSecret: String,
    code: String,
    redirectUri: String /*
    username: String = "",
    password: String = ""*/ )
    extends GenerateTokenReq

case class GenerateRefreshTokenReq(
    grantType: GrantType,
    clientId: String,
    clientSecret: String,
    refreshToken: String,
    scopes: Set[String])
    extends GenerateTokenReq {
  require(
    GrantType.REFRESH_TOKEN == grantType,
    s"The required grant type is 'refresh_token', but the incoming is '$grantType'.")
}

object AccessTokens {
  def toGenerateTokenReq(query: Map[String, String], authorization: Option[Authorization]): GenerateTokenReq = {
    val grantType: GrantType = query.get("grant_type") match {
      case Some(grantType) => GrantType.string2GrantType(grantType)
      case _ =>
        query
          .get("response_type")
          .map(ResponseType.string2ResponseType)
          .collect {
            case ResponseType.CODE => GrantType.AUTHORIZATION_CODE
          }
          .getOrElse(throw HSBadRequestException("Invalid grant type or dose not exists."))
    }
    val redirectUri = query.getOrElse("redirect_uri", "")
    val (clientId, clientSecret) = authorization
      .collect {
        case Authorization(BasicHttpCredentials(username, password)) => username -> password
      }
      .orElse {
        for {
          clientId <- query.get("client_id")
          clientSecret <- query.get("client_secret")
        } yield clientId -> clientSecret
      }
      .getOrElse(throw HSBadRequestException(
        "Please submit the 'client_id' and 'client_secret' parameters via HTTP BasicCredentials or FormData."))

    grantType match {
      case GrantType.REFRESH_TOKEN =>
        val refreshToken = query("refresh_token")
        val scopes = query.getOrElse("scope", "").split(' ').view.map(_.trim).filter(_.nonEmpty).toSet
        GenerateRefreshTokenReq(grantType, clientId, clientSecret, refreshToken, scopes)
      case _ =>
        val code = query("code")
        GenerateAccessTokenReq(grantType, clientId, clientSecret, code, redirectUri)
    }
  }
}
