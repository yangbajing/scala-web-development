package scalaweb.oauth

/**
 * 通过 cookie 方式保存的会话令牌
 * @author Yang Jing <a href="mailto:yang.xunjing@qq.com">yangbajing</a>
 * @since 2021-12-20 21:01:22
 */
case class AccessSession(id: String, expiresIn: Long, createTime: Long)

case class LoginReq(username: String, password: String, redirectUri: String, oauth2ReqId: String)

/**
 * @param token 会话令牌
 */
case class LoginResp(token: String)
